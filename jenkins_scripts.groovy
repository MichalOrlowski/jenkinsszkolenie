import groovy.json.JsonOutput

def deploy() {
	deployPackage()
	notifySlack()
}

def deployPackage() {
	echo "Deploying package..."
}

def notifySlack() {
	echo "Noti slack..."
	stage('Notify Slack') {
		generateSlackPayload()
   }
}


def generateSlackPayload() {
	bat "curl -X POST --data-urlencode \"payload={\\\"channel\\\": \\\"#test\\\", \\\"username\\\": \\\"webhookbot\\\", \\\"text\\\": \\\"This is posted to #test and comes from a bot named webhookbot.\\\", \\\"icon_emoji\\\": \\\":ghost:\\\"}\" https://hooks.slack.com/services/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"
}

return this


