node {
	checkout scm
	
	def script = load 'jenkins_scripts.groovy'
	
	try {
		script.deploy()
	} finally {
		cleanWs()
	}
}